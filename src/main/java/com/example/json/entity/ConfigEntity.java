package com.example.json.entity;

public class ConfigEntity {
    private String editorHost;
    private String editorCaller;
    private String apiKey;

    public ConfigEntity(String editorHost, String editorCaller, String apiKey) {
        this.editorHost = editorHost;
        this.editorCaller = editorCaller;
        this.apiKey = apiKey;
    }

    public String getEditorHost() {
        return editorHost;
    }

    public void setEditorHost(String editorHost) {
        this.editorHost = editorHost;
    }

    public String getEditorCaller() {
        return editorCaller;
    }

    public void setEditorCaller(String editorCaller) {
        this.editorCaller = editorCaller;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }


}
